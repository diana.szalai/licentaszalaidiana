//
//  MealPlannerForm.swift
//  ListUsers
//
//  Created by Diana Szalai on 18.06.2023.
//

import SwiftUI

struct MealPlannerFormScreen: View {
    enum FocusFields {
        case numberOfDays, numberOfMeals, numberOfCalories, numberOfProteins
    }
    @ObservedObject var viewModel: MealPlannerFormScreenViewModel
    @FocusState var focusedField: FocusFields?
    @Environment(\.colorScheme) var colorScheme
    
    init(viewModel: MealPlannerFormScreenViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        Form(content: {
            Section(header: Text("Indications")) {
                TextField("Number Of Days", text: $viewModel.numberOfDays)
                    .disableAutocorrection(true)
                    .submitLabel(.next)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .numberOfDays)
                    .showClearButton($viewModel.numberOfDays, isHidden: focusedField != .numberOfDays)
                    .onSubmit {
                        focusedField = .numberOfDays
                    }
                TextField("Number Of Meals", text: $viewModel.numberOfMeals)
                    .disableAutocorrection(true)
                    .submitLabel(.next)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .numberOfMeals)
                    .showClearButton($viewModel.numberOfMeals, isHidden: focusedField != .numberOfMeals)
                    .onSubmit {
                        focusedField = .numberOfMeals
                    }
                TextField("Number Of Calories", text: $viewModel.numberOfCalories)
                    .disableAutocorrection(true)
                    .submitLabel(.next)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .numberOfCalories)
                    .showClearButton($viewModel.numberOfCalories, isHidden: focusedField != .numberOfCalories)
                    .onSubmit {
                        focusedField = .numberOfCalories
                    }
                TextField("Number Of Calories", text: $viewModel.numberOfProteins)
                    .disableAutocorrection(true)
                    .submitLabel(.next)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .numberOfProteins)
                    .showClearButton($viewModel.numberOfProteins, isHidden: focusedField != .numberOfProteins)
                    .onSubmit {
                        focusedField = nil
                    }
            }
            // Button
            if viewModel.recipeResponse.isEmpty {
                Button(action: {
                    Task { @MainActor in
                        await viewModel.sendAction()
                    }
                }) {
                    HStack {
                        Spacer()
                        Text("Generate Meal Planner")
                        Spacer()
                    }
                }
                .foregroundColor(.white)
                .padding(10)
                .background(Color.accentColor)
                .cornerRadius(8)
            } else {
                Button(action: {
                    Task { @MainActor in
                        await viewModel.retry()
                    }
                }) {
                    HStack {
                        Spacer()
                        Text("Retry Generate Meal Planner")
                        Spacer()
                    }
                }
                .foregroundColor(.white)
                .padding(10)
                .background(Color.accentColor)
                .cornerRadius(8)
            }
            
            Button(action: {
                viewModel.openFormDetailAction(viewModel.recipeResponse)
            }) {
                Text("Show Meal Planner")
            }
            .foregroundColor(Color.accentColor)
        })
        .background(colorScheme == .light ? .white : Color(red: 52/255, green: 53/255, blue: 65/255, opacity: 0.5))
        .navigationBarTitle("Meal Planner")
    }
}
