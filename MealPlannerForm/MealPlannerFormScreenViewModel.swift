//
//  MealPlannerFormScreenViewModel.swift
//  ListUsers
//
//  Created by Diana Szalai on 18.06.2023.
//

import Combine
import Foundation

class MealPlannerFormScreenViewModel: ObservableObject {
    
    @Published var numberOfDays: String = ""
    @Published var numberOfMeals: String = ""
    @Published var numberOfCalories: String = ""
    @Published var numberOfProteins: String = ""
    @Published var recipeResponse: [MessageRow] = []
    @Published var inputMessage: String = ""
    @Published var viewState: ViewState = .default
    
    private weak var mealFormCoordinator: MealFormCoordinator?
    private let api: ChatGPTAPI
    private var task: Task<Void, Never>?
    
    init(api: ChatGPTAPI,
         mealFormCoordinator: MealFormCoordinator?) {
        self.api = api
        self.mealFormCoordinator = mealFormCoordinator
    }
    
    @MainActor
    func retry() async {
       let index = recipeResponse.startIndex
        self.recipeResponse.remove(at: index)
        
        self.task = Task {
            inputMessage = "Create a meal planner that has a \(numberOfMeals) number of meals per day, for a period of \(numberOfDays). The meal prep. should have around \(numberOfCalories) calories and around \(numberOfProteins) proteins."
            let text = inputMessage
            inputMessage = ""
            await send(text: text)
        }
    }
    
    @MainActor
    func sendAction() async {
        self.task = Task {
            inputMessage = "Create a meal planner that has a \(numberOfMeals) number of meals per day, for a period of \(numberOfDays). The meal prep. should have around \(numberOfCalories) calories and around \(numberOfProteins) proteins."
            let text = inputMessage
            inputMessage = ""
            await send(text: text)
        }
    }
    
    private func send(text: String) async {
        let streamText = ""
        var messageRow = MessageRow(
            isInteracting: true,
            sendImage: "profile",
            send: .rawText(text),
            responseImage: "openai",
            response: .rawText(streamText),
            responseError: nil)
        
        do {
            let stream = try await api.sendMessage(text)
            messageRow.response = .rawText(stream)
            self.recipeResponse.append(messageRow)
            
        } catch {
            messageRow.responseError = error.localizedDescription
        }
    }
    
    func openFormDetailAction(_ response: [MessageRow]) {
        mealFormCoordinator?.showMealPrepDetails(response: response)
    }
}
