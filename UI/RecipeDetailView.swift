//
//  RecipeDetailView.swift
//  FoodRecipes
//
//  Created by DevTechie on 3/19/22.
//

import SwiftUI

public struct RecipeDetailView: View {
    var recipe: RecipeInfo
    @Binding var isShowing: Bool
    @Environment(\.dismiss) var dismiss
    
    public init(recipe: RecipeInfo,
                isShowing: Binding<Bool>) {
        
        self.recipe = recipe
        self._isShowing = isShowing
    }
    
    @ViewBuilder
    private var backButton: some View {
        HStack {
            Button { dismiss() } label: {
                Image(systemName: "chevron.backward")
                    .font(.title3)
                    .foregroundColor(.primary)
                    .padding()
                    .background(.thinMaterial, in: Circle())
            }
            Spacer()
        }
        .padding(.top, 40)
        .padding(.leading, 20)
    }
    
    public var body: some View {
        VStack {
            ZStack {
                BackgroundImage(image: Image(recipe.imageName))
                VStack {
                    backButton
                    Spacer()
                    RecipeByView()
                }
            }
            List {
                VStack(alignment: .leading, spacing: 10) {
                    DescHeaderView(stepsShowing: $isShowing, recipe: recipe)
                    if isShowing {
                        StepsView(steps: recipe.steps)
                    } else {
                        IngredientsView(ingredients: recipe.ingredients)
                    }
                }
                .padding(.top)
            }.listStyle(.plain)
        }
        .navigationBarHidden(true)
        .ignoresSafeArea()
        .padding([.bottom], 20)
    }
}
