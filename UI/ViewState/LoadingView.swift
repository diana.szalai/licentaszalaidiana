//
//  LoadingView.swift
//  ListUsers
//
//  Created by Diana Szalai on 16.06.2023.
//

import SwiftUI

struct LoadingView: View {
    let text: String
    
    init(text: String) {
        self.text = text
    }
    
    var body: some View {
        GeometryReader { geometry in
            HStack {
                Text(self.text)
                    .font(.subheadline)
                    .foregroundColor(.gray)
                ProgressView()
            }
            .frame(width: geometry.size.width,
                   height: geometry.size.height,
                   alignment: .center)
            .padding(.top ,geometry.size.height/4)
        }
    }
}
