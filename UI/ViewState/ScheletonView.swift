//
//  ScheletonView.swift
//  ListUsers
//
//  Created by Diana Szalai on 18.06.2023.
//

import SwiftUI

struct ScheletonView: View {
    let text: String
    
    init(text: String) {
        self.text = text
    }
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                Text(self.text)
                    .font(.subheadline)
                    .foregroundColor(.gray)
                Text(self.text)
                    .font(.subheadline)
                    .foregroundColor(.gray)
                Text(self.text)
                    .font(.subheadline)
                    .foregroundColor(.gray)
                Text(self.text)
                    .font(.subheadline)
                    .foregroundColor(.gray)
//                ProgressView()
            }
            .frame(width: geometry.size.width,
                   height: geometry.size.height,
                   alignment: .center)
            .padding(.top ,geometry.size.height/4)
        }
    }
}
