//
//  NoResultsView.swift
//  ListUsers
//
//  Created by Diana Szalai on 16.06.2023.
//

import SwiftUI


struct NoResultsView: View {
    var body: some View {
        GeometryReader { geometry in
            Text("NoResultsView.Description")
                .font(.title)
                .foregroundColor(.gray)
            .frame(width: geometry.size.width,
                   height: geometry.size.height,
                   alignment: .center)
        }
    }
}
