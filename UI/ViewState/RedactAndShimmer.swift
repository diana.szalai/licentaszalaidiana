//
//  RedactAndShimmer.swift
//  ListUsers
//
//  Created by Diana Szalai on 16.06.2023.
//

import SwiftUI
import Foundation

public struct RedactAndShimmer: ViewModifier {
    private let condition: Bool
    
    init(condition: Bool) {
        self.condition = condition
    }
    
    public func body(content: Content) -> some View {
        if condition {
            content
                .redacted(reason: .placeholder)
                .shimmering()
        } else {
            content
        }
    }
}

extension View {
    public func redactShimmer(condition: Bool) -> some View {
        modifier(RedactAndShimmer(condition: condition))
    }
}
