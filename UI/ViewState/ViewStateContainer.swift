//
//  ViewStateContainer.swift
//  ListUsers
//
//  Created by Diana Szalai on 16.06.2023.


import SwiftUI

public enum ViewState: Equatable {
    case `default`
    case redactAndShimmer
    case loading(text: String)
    case generalLoading
    case noResultsFound
}

public protocol ViewStateProvider: AnyObject {
    var viewState: ViewState { get set }
}

public extension View {
    func viewState(_ state: Binding<ViewState>) -> some View {
        modifier(ViewStateModifier(state: state))
    }
}

fileprivate struct ViewStateModifier: ViewModifier {
    @Binding var state: ViewState
    
    func body(content: Content) -> some View {
        ViewStateContainer(state: $state, mainView: { content })
    }
}

fileprivate struct ViewStateContainer<MainView: View>: View {
    @Binding var state: ViewState
    let mainView: () -> MainView
    
    var body: some View {
        content
    }
    
    @ViewBuilder
    var content: some View {
        switch state {
        case .default:
            mainView()
        case .redactAndShimmer:
            ScheletonView(text: "General.Loading")
                .redactShimmer(condition: true)
                .disabled(true)
        case let .loading(text):
            LoadingView(text: text)
        case .generalLoading:
            LoadingView(text: "General.Loading")
        case .noResultsFound:
            NoResultsView()
        }
    }
    
}

