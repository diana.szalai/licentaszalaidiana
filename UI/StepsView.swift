//
//  StepsView.swift
//  ListUsers
//
//  Created by Diana Szalai on 17.06.2023.
//

import SwiftUI

struct StepsView: View {
    var steps: [String]
    
    init(steps: [String]) {
        self.steps = steps
    }
    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .lastTextBaseline) {
                Text("Steps")
                    .font(.title2)
                    .padding(.top, 40)
                Spacer()
                Text("\(steps.count) steps")
            }
            
            ForEach(steps, id: \.self) { step in
                Text(step)
                    .padding(.vertical)
            }
        }
    }
}
