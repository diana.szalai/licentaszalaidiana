//
//  DescHeaderView.swift
//  ListUsers
//
//  Created by Diana Szalai on 17.06.2023.
//

import SwiftUI


struct DescHeaderView: View {
    @Binding var stepsShowing: Bool
    let recipe: RecipeInfo
    
    init(stepsShowing: Binding<Bool>,
         recipe: RecipeInfo){
        self._stepsShowing = stepsShowing
        self.recipe = recipe
    }
    public var body: some View {
        VStack {
            VStack {
                Text(recipe.name.capitalized)
                    .font(.title2)
                    
                Text("\(recipe.cookTime) | \(recipe.servings)")
                    .foregroundColor(.secondary)
                Picker("", selection: $stepsShowing) {
                    Text("Ingredinets")
                        .tag(false)
                    Text("Steps")
                        .tag(true)
                }.labelsHidden()
                    .pickerStyle(.segmented)
            }
        }
    }
}

struct DescHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        DescHeaderView(stepsShowing: .constant(false), recipe: Recipe.sampleData[0])
    }
}
