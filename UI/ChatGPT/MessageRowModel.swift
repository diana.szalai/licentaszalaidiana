//
//  MessageRowModel.swift
//  ListUsers
//
//  Created by Diana Szalai on 12.06.2023.
//

import Foundation


struct AttributedOutput {
    let string: String
    let results: [ParserResult]
}

enum MessageRowType {
    case attributed(AttributedOutput)
    case rawText(String)
    
    var text: String {
        switch self {
        case .attributed(let attributedOutput):
            return attributedOutput.string
        case .rawText(let string):
            return string
        }
    }
}

struct MessageRow: Identifiable {
    
    let id = UUID()
    var isInteracting: Bool
    
    let sendImage: String
    var send: MessageRowType
    var sendText: String {
        send.text
    }
    
    let responseImage: String
    var response: MessageRowType?
    var responseText: String? {
        response?.text
    }
    
    var responseError: String?
}

// MARK: - Equatable
extension MessageRow: Equatable, Hashable {
    public func hash(into hasher: inout Hasher) {
        return hasher.combine(id)
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
}
