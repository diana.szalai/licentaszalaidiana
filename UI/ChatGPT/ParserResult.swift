//
//  ParserResult.swift
//  ListUsers
//
//  Created by Diana Szalai on 13.06.2023.
//

import SwiftUI

struct ParserResult: Identifiable {
    
    let id = UUID()
    let attributedString: AttributedString
    let isCodeBlock: Bool
    let codeBlockLanguage: String?
    
}
