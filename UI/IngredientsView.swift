//
//  IngredientsView.swift
//  ListUsers
//
//  Created by Diana Szalai on 17.06.2023.
//

import SwiftUI

struct IngredientsView: View {
    var ingredients: [Ingredient]
    
    init(ingredients: [Ingredient]) {
        self.ingredients = ingredients
    }
    
    var body: some View {
        VStack {
            HStack(alignment: .lastTextBaseline) {
                Text("Ingredients")
                    .font(.title2)
                    .padding(.top, 40)
                
                Spacer()
                
                Text("\(ingredients.count) item")
                    .font(.callout)
                    .foregroundColor(.secondary)
            }
            
            ForEach(ingredients, id: \.self) { ingredient in
                HStack(alignment: .center) {
                    Text(ingredient.emoji)
                        .padding(10)
                        .background(.thinMaterial, in: RoundedRectangle(cornerRadius: 10))
                    Text(ingredient.name)
                    Spacer()
                    Text(ingredient.quantity)
                        .multilineTextAlignment(.trailing)
                }
                .padding(.vertical)
            }
        }
    }
}

//struct IngredientsView_Previews: PreviewProvider {
//    static var previews: some View {
//        IngredientsView(recipe: Recipe.sampleData[1])
//    }
//}

