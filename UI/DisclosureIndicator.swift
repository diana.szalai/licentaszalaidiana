//
//  File.swift
//  ListUsers
//
//  Created by Diana Szalai on 16.06.2023.
//

import SwiftUI

public struct DisclosureIndicator: View {
    
    // periphery:ignore
    public enum Direction {
        case right, left
    }
    
    private let pointingDirection: Direction
    private let foregroundColor: Color
    
    public init(pointingDirection: Direction = .right, foregroundColor: Color = Color.gray.opacity(0.1)) {
        self.pointingDirection = pointingDirection
        self.foregroundColor = foregroundColor
    }
    
    public var body: some View {
        (pointingDirection == .right ? Image.disclosureRight : Image.disclosureLeft)
            .font(.headline)
            .foregroundColor(foregroundColor)
    }
}

public extension Image {
    static let disclosureLeft = Image(systemName: "chevron.left")
    static let disclosureRight = Image(systemName: "chevron.right")
}
