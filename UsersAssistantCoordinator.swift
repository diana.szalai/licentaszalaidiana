//
//  ListUsersCoordinator.swift
//  ListUsers
//
//  Created by Diana Szalai on 06.06.2023.
//
import Foundation
import SwiftUI


/// Stable identifier of the routes that are accessible in the `Route` feature flow.
enum Route: Hashable {
    /// Details page of an item in which more details are visible and rating page.
    case details(user: User, index: Int, users: [User])
}

class UsersAssistantCoordinator: ObservableObject {
    
    // MARK: - Properties
    private(set) var usersAssistantScreenViewModel: UsersAssistantScreenViewModel?
    private(set) var chatGPTViewModel: ChatGPTViewModel
    private(set) var chatGPTAPI: ChatGPTAPI
    
    /// Navigation
    @Published var navigationPath: NavigationPath = .init()
    
    // MARK: - Init
    
    init(chatGPTAPI: ChatGPTAPI) {
        
        self.chatGPTAPI = chatGPTAPI
        self.chatGPTViewModel = ChatGPTViewModel(api: chatGPTAPI)
        self.usersAssistantScreenViewModel = UsersAssistantScreenViewModel(
            api: chatGPTAPI,
            usersAssistantCoordinator: self,
            chatGPTViewModel: chatGPTViewModel)
    }

    // MARK: Methods to be trigger when a certain event occurs
    
    func openUserDetails(for user: User, index: Int, users: [User]) {
        navigationPath.append(Route.details(user: user, index: index, users: users))
    }
}
