//
//  MealPlannerDetailsScreen.swift
//  ListUsers
//
//  Created by Diana Szalai on 18.06.2023.
//

import SwiftUI

struct MealPlannerDetailScreen: View {
    @ObservedObject var viewModel: MealPlannerDetailScreenViewModel
    @State private var stepsShowing = false
    @Environment(\.colorScheme) var colorScheme
    
    init(viewModel: MealPlannerDetailScreenViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ScrollView {
            ForEach(viewModel.formResponse) { message in
                if let response = message.response {
                    responseFormRow(
                        rowType: response,
                        bgColor: .gray.opacity(0.1)
                    )
                } else {
                    ScheletonView(text: "General Loading")
                        .redactShimmer(condition: true)
                        .disabled(true)
                }
            }
        }.background(colorScheme == .light ? .white : Color(red: 52/255, green: 53/255, blue: 65/255, opacity: 0.5))
    }
}

func responseFormRow(rowType: MessageRowType,
                        bgColor: Color) -> some View {
    HStack(alignment: .top, spacing: 8) {
        VStack(alignment: .leading) {
            if !rowType.text.isEmpty {
                Text(rowType.text)
                    .multilineTextAlignment(.leading)
                    .textSelection(.enabled)
            }
        }
    }
    .padding(12)
    .frame(maxWidth: .infinity, alignment: .leading)
    .background(bgColor)
}

