//
//  MealPlannerDetailsScreenViewModel.swift
//  ListUsers
//
//  Created by Diana Szalai on 18.06.2023.
//

import Combine
import Foundation

class MealPlannerDetailScreenViewModel: ObservableObject {
    
    @Published var formResponse: [MessageRow] = []
    @Published var finalResponse: [String] = []
    @Published var viewState: ViewState = .default
    
    private weak var mealFormCoordinator: MealFormCoordinator?
    private var task: Task<Void, Never>?
    private var cancellableSet: Set<AnyCancellable> = []
    
    init(formResponse: [MessageRow],
         mealFormCoordinator: MealFormCoordinator?) {
        
        self.formResponse = formResponse
        self.mealFormCoordinator = mealFormCoordinator
        
        if formResponse.isEmpty {
            viewState = .redactAndShimmer
        } else {
            viewState = .default
        }
        
        editResponse()
    }
    
    func editResponse() {
        formResponse.forEach{
            self.finalResponse.append( $0.response?.text ?? "")
        }
    }
}
