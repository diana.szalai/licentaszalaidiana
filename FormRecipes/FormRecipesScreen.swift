//
//  FormRecipesScreen.swift
//  ListUsers
//
//  Created by Diana Szalai on 15.06.2023.
//

import SwiftUI

enum Difficulty: String, CaseIterable, Identifiable {
    case easy
    case mediu
    case hard
    var id: String { self.rawValue }
}
enum RecipeTypes: String, CaseIterable, Identifiable {
    case breakfast
    case brunch
    case lunch
    case dinner
    case snacks
    case other
    var id: String { self.rawValue }
}
enum DietTypes: String, CaseIterable, Identifiable {
    case bio
    case vegetarian
    case vegan
    case keto
    var id: String { self.rawValue }
}

struct FormRecipesScreen: View {
    enum FocusFields {
        case name, ingredients, exceptIngredients, caloriesMin, caloriesMax, cookTime, servings
    }
    
    @ObservedObject var viewModel: FormRecipesScreenViewModel
    @FocusState var focusedField: FocusFields?
    @Environment(\.colorScheme) var colorScheme
    
    init(viewModel: FormRecipesScreenViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        Form(content: {
            Section(header: Text("Ingredients")) {
                TextField("Recipe Name", text: $viewModel.name)
                    .disableAutocorrection(true)
                    .submitLabel(.next)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .name)
                    .showClearButton($viewModel.name, isHidden: focusedField != .name)
                    .onSubmit {
                        focusedField = .name
                    }
                TextField("Ingredients", text: $viewModel.ingredients)
                    .disableAutocorrection(true)
                    .submitLabel(.next)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .ingredients)
                    .showClearButton($viewModel.ingredients, isHidden: focusedField != .ingredients)
                    .onSubmit {
                        focusedField = .ingredients
                    }
                TextField("Don't like Ingredients", text: $viewModel.exceptIngredients)
                    .disableAutocorrection(true)
                    .submitLabel(.next)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .exceptIngredients)
                    .showClearButton($viewModel.exceptIngredients, isHidden: focusedField != .exceptIngredients)
                    .onSubmit {
                        focusedField = .exceptIngredients
                    }
            }
            Section(header: Text("Recipe Information")){
                TextField("Calories min", text: $viewModel.caloriesMin)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .caloriesMin)
                    .showClearButton($viewModel.caloriesMin, isHidden: focusedField != .caloriesMin)
                    .onSubmit {
                        focusedField = nil
                    }
                TextField("Calories max", text: $viewModel.caloriesMax)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .caloriesMax)
                    .showClearButton($viewModel.caloriesMax, isHidden: focusedField != .caloriesMax)
                    .onSubmit {
                        focusedField = .cookTime
                    }
                TextField("Recipe cooktime", text: $viewModel.cookTime)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .cookTime)
                    .showClearButton($viewModel.cookTime, isHidden: focusedField != .cookTime)
                    .onSubmit {
                        focusedField = .servings
                    }
                TextField("Recipe servings", text: $viewModel.servings)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .focused($focusedField, equals: .servings)
                    .showClearButton($viewModel.cookTime, isHidden: focusedField != .servings)
                    .onSubmit {
                        focusedField = nil
                    }
            }
            Section(header: Text("types of recipes")) {
                // Segment Picker
                Picker("Preparation", selection: $viewModel.preparation) {
                    ForEach(Difficulty.allCases) { type in
                        Text(type.rawValue.capitalized).tag(type)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                
                // Scroll picker
                Picker("Types of recipes", selection: $viewModel.recipe) {
                    ForEach(RecipeTypes.allCases) { recipe in
                        Text(recipe.rawValue.capitalized).tag(recipe)
                    }
                }
                Picker("Types of diet", selection: $viewModel.diet) {
                    ForEach(DietTypes.allCases) { diet in
                        Text(diet.rawValue.capitalized).tag(diet)
                    }
                }
            }
            // Button
            if viewModel.recipeResponse != nil {
                Button(action: {
                    Task { @MainActor in
                        await viewModel.sendAction()
                    }
                }) {
                    HStack {
                        Spacer()
                        Text("Search for Recipe!")
                        Spacer()
                    }
                }
                .foregroundColor(.white)
                .padding(10)
                .background(Color.accentColor)
                .cornerRadius(8)
            } else {
                Button(action: {
                    Task { @MainActor in
                        await viewModel.retry()
                    }
                }) {
                    HStack {
                        Spacer()
                        Text("Retry Recipe")
                        Spacer()
                    }
                }
                .foregroundColor(.white)
                .padding(10)
                .background(Color.accentColor)
                .cornerRadius(8)
            }
            
            Button(action: {
                viewModel.openRecipeDetailAction(viewModel.recipeResponse,
                                                 viewModel.name,
                                                 viewModel.cookTime,
                                                 viewModel.servings)
            }) {
                Text("Show Recipe")
            }.foregroundColor(Color.accentColor)
        })
        .background(colorScheme == .light ? .white : Color(red: 52/255, green: 53/255, blue: 65/255, opacity: 0.5))
        .navigationBarTitle("Recipe Form")
    }
}

struct TextFieldClearButton: ViewModifier {
    @Binding var fieldText: String
    @Environment(\.colorScheme) var colorScheme
    var isHidden: Bool
    
    func body(content: Content) -> some View {
        HStack {
            content
            if !fieldText.isEmpty && !isHidden {
                Spacer()
                Image(systemName: "multiply.circle.fill")
                    .foregroundColor(.secondary)
                    .background(.white)
                    .padding(.trailing, 4)
                    .onTapGesture {
                        fieldText = ""
                    }
            }
        }
    }
}

extension View {
    public func showClearButton(_ text: Binding<String>,
                                isHidden: Bool) -> some View {
        modifier(TextFieldClearButton(fieldText: text, isHidden: isHidden))
    }
    
    func placeholder<Content: View>(
        when shouldShow: Bool,
        alignment: Alignment = .leading,
        @ViewBuilder placeholder: () -> Content) -> some View {
            
            ZStack(alignment: alignment) {
                placeholder().opacity(shouldShow ? 1 : 0)
                self
            }
        }
    
    public func placeholder(
        _ text: String,
        when shouldShow: Bool,
        alignment: Alignment = .leading) -> some View {
            
            placeholder(when: shouldShow, alignment: alignment) { Text(text).foregroundColor(.gray) }
        }
}

