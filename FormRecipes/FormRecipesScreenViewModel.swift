//
//  FormRecipesScreenViewModel.swift
//  ListUsers
//
//  Created by Diana Szalai on 15.06.2023.
//

import Combine
import Foundation

class FormRecipesScreenViewModel: ObservableObject {
    
    @Published var name: String = ""
    @Published var ingredients: String = ""
    @Published var exceptIngredients: String = ""
    @Published var caloriesMin: String = ""
    @Published var caloriesMax: String = ""
    @Published var cookTime: String = ""
    @Published var servings: String = ""
    @Published var preparation: Difficulty = .easy
    @Published var recipe: RecipeTypes = .breakfast
    @Published var diet: DietTypes = .bio
    @Published var recipeResponse: [MessageRow] = []
    @Published var inputMessage: String = ""
    @Published var viewState: ViewState = .default
    
    private weak var formCoordinator: FormCoordinator?
    var recipeDetailScreenViewModel: RecipeDetailScreenViewModel?
    private let recipeRepository: RecipeRepository
    private var task: Task<Void, Never>?
    
    init(recipeRepository: RecipeRepository,
         formCoordinator: FormCoordinator?) {
        
        self.recipeRepository = recipeRepository
        self.formCoordinator = formCoordinator
    }
    
    @MainActor
    func retry() async {
        let index = recipeResponse.startIndex
        self.recipeResponse.remove(at: index)
        
        self.task = Task {
            inputMessage = "Create a recipe for \(recipe) with the following ingredients: \(ingredients). Add emoji for ingredients. Exclude the following ingredinets: \(exceptIngredients). The recipe should contain around \(caloriesMin) - \(caloriesMax), and the preparation should be \(preparation). The recepie should respect the \(diet) diet. The recipe should be for \(servings) servings, and the cook time should be: \(cookTime). Group it in 3 main categories, ingredients, preparation  and name"
            let text = inputMessage
            inputMessage = ""
            await send(text: text,
                       recipeName: name,
                       servings: servings,
                       cookTime: cookTime,
                       recipeType: "\(recipe)")
        }
    }
    
    @MainActor
    func sendAction() async {
        self.task = Task {
            inputMessage = "Create a recipe for \(recipe) with the following ingredients: \(ingredients). Add emoji for ingredients. Exclude the following ingredinets: \(exceptIngredients). The recipe should contain around \(caloriesMin) - \(caloriesMax), and the preparation should be \(preparation). The recepie should respect the \(diet) diet. The recipe should be for \(servings) servings, and the cook time should be: \(cookTime). Group it in 3 main categories, ingredients, preparation  and name"
            let text = inputMessage
            inputMessage = ""
            await send(text: text,
                       recipeName: name,
                       servings: servings,
                       cookTime: cookTime,
                       recipeType: "\(recipe)"
            )
        }
    }
    
    private func send(text: String,
                      recipeName: String,
                      servings: String,
                      cookTime: String,
                      recipeType: String) async {
        let streamText = ""
        
        var messageRow = MessageRow(
            isInteracting: true,
            sendImage: "profile",
            send: .rawText(text),
            responseImage: "openai",
            response: .rawText(streamText),
            responseError: nil)
        
        do {
            let stream = await recipeRepository.getRecipe(text: text,
                                                          recipeName: recipeName,
                                                          servings: servings,
                                                          cookTime: cookTime,
                                                          recipeType: recipeType)
            messageRow.response = .rawText(stream?.responseText ?? "")
            self.recipeResponse.append(messageRow)
        }
    }
    
    func openRecipeDetailAction(_ recipe: [MessageRow], _ recipeName: String, _ cookTime: String, _ servings: String) {
        formCoordinator?.showRecipeDetails(recipeResponse: recipe,
                                           recipeName: recipeName,
                                           cookTime: cookTime,
                                           servings: servings)
    }
}
