//
//  SearchCoordinator.swift
//  FoodRecipes
//
//  Created by Diana Szalai on 22.06.2023.
//

import Foundation
import SwiftUI

enum SearchRoute: Hashable {
    /// Details page of an item in which more details are visible and rating page.
    case recipeListDetails(recipeResponse: [MessageRow], recipeName: String, cookTime: String, servings: String)
}

class SearchCoordinator: ObservableObject {
    
    // MARK: - Properties
    private(set) var recipeListScreenViewModel: RecipeListScreenViewModel?
    private(set) var recipeRepository: RecipeRepository
    private(set) var formCoordinator: FormCoordinator

    /// Navigation
    @Published var navigationPath: NavigationPath = .init()
    
    // MARK: - Init
    init(recipeRepository: RecipeRepository,
         formCoordinator: FormCoordinator) {
        
        self.recipeRepository = recipeRepository
        self.formCoordinator = formCoordinator
        self.recipeListScreenViewModel = RecipeListScreenViewModel(
            recipeRepository: recipeRepository,
            searchCoordinator: self,
            formCoordinator: formCoordinator)
    }

    // MARK: Methods to be trigger when a certain event occurs
    func showRecipeSearch(recipeResponse: [MessageRow],
                          recipeName: String,
                          cookTime: String,
                          servings: String) {
        navigationPath.append(
            SearchRoute.recipeListDetails(
                recipeResponse: recipeResponse,
                recipeName: recipeName,
                cookTime: cookTime,
                servings: servings)
        )
    }
}
