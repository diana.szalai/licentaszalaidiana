//
//  SearchCoordinatorView.swift
//  FoodRecipes
//
//  Created by Diana Szalai on 22.06.2023.
//

import SwiftUI

struct SearchCoordinatorView: View {
    
    @ObservedObject var searchCoordinator: SearchCoordinator
    
    // MARK: Views
    var body: some View {
        NavigationStack(path: $searchCoordinator.navigationPath) {
            if let viewModel = searchCoordinator.recipeListScreenViewModel {
                RecipeListView(viewModel: viewModel)
                    .handleNavigation(coordinator: searchCoordinator)
            }
        }
    }
}

extension View {
    func handleNavigation(coordinator: SearchCoordinator) -> some View {
        navigationDestination(for: SearchRoute.self) { route in
            switch route {
            case .recipeListDetails(let recipe, let recipeName, let cookTime, let servings):
                let viewModel = RecipeDetailScreenViewModel(
                    recipeResponse: recipe,
                    recipeName: recipeName,
                    cookTime: cookTime,
                    servings: servings,
                    formCoordinator: coordinator.formCoordinator)
                RecipeDetailScreen(viewModel: viewModel)
            }
        }
    }
}
