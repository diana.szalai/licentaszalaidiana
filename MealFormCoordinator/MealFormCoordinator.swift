//
//  MealFormCoordinator.swift
//  ListUsers
//
//  Created by Diana Szalai on 18.06.2023.
//

import Foundation
import SwiftUI

enum MealFormRoute: Hashable {
    /// Details page of an item in which more details are visible and rating page.
    case mealFormDetails(response: [MessageRow])
}

class MealFormCoordinator: ObservableObject {
    
    // MARK: - Properties
    private(set) var mealPlannerFormScreenViewModel: MealPlannerFormScreenViewModel?
  
    private(set) var chatGPTAPI: ChatGPTAPI

    /// Navigation
    @Published var navigationPath: NavigationPath = .init()
    
    // MARK: - Init
    
    init(chatGPTAPI: ChatGPTAPI) {
           
        self.chatGPTAPI = chatGPTAPI
        self.mealPlannerFormScreenViewModel = MealPlannerFormScreenViewModel(
            api: chatGPTAPI,
            mealFormCoordinator: self)
    }

    // MARK: Methods to be trigger when a certain event occurs
    
    func showMealPrepDetails(response: [MessageRow]) {
        navigationPath.append(MealFormRoute.mealFormDetails(response: response))
    }
}

