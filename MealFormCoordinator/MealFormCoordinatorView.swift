//
//  MealFormCoordinatorView.swift
//  ListUsers
//
//  Created by Diana Szalai on 18.06.2023.
//

import SwiftUI

struct MealFormCoordinatorView: View {
    
    @ObservedObject var mealFormCoordinator: MealFormCoordinator
    
    // MARK: Views
    var body: some View {
        NavigationStack(path: $mealFormCoordinator.navigationPath) {
            if let viewModel = mealFormCoordinator.mealPlannerFormScreenViewModel {
                MealPlannerFormScreen(viewModel: viewModel)
                    .handleMealPrepFormNavigation(coordinator: mealFormCoordinator)
            }
        }
    }
}

extension View {
    func handleMealPrepFormNavigation(coordinator: MealFormCoordinator) -> some View {
        navigationDestination(for: MealFormRoute.self) { route in
            switch route {
            case .mealFormDetails(let response):
                let viewModel = MealPlannerDetailScreenViewModel(formResponse: response,
                                                                 mealFormCoordinator: coordinator)
                MealPlannerDetailScreen(viewModel: viewModel)
            }
        }
    }
}
