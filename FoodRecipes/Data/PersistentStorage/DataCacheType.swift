//
//  DataCacheType.swift
//  ListUsers
//
//  Created by Diana Szalai on 29.05.2023.
//

import Foundation
import RealmSwift

protocol DataCacheType {
    
    var provider: RealmProviderType { get }
    
    /**
        Single object realm updates
     */
    func get<T: Object>(id: Any) -> T?
    func delete<T: Object>(object: T) -> Bool
    func delete<T: Object>(id: Any, of type: T.Type) -> Bool
    
    // Object collections
    func addOrUpdate<T: Object>(objects: [T]) -> Bool
    func getAll<T: Object>() -> Results<T>?
    func delete<T: Object>(objects: [T]) -> Bool
    func deleteAll<T: Object>(of type: T.Type) -> Bool
}
