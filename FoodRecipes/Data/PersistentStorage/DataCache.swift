//
//  DataCache.swift
//  ListUsers
//
//  Created by Diana Szalai on 29.05.2023.
//

import Foundation
import RealmSwift

class DataCache: DataCacheType {
    
    // MARK: - Properties
    
    private(set) var provider: RealmProviderType
    
    // MARK: - Init
    
    required init(provider: RealmProviderType) {
        self.provider = provider
    }
    
    // MARK: - DataCacheType
    
    func addOrUpdate<T: Object>(newObject: T) -> Bool {
        do {
            let realm = try Realm(configuration: self.provider.configuration)
            try realm.write {
                if T.primaryKey() != nil {
                    realm.add(newObject, update: .all)
                } else {
                    realm.add(newObject, update: .error)
                }
            }
            return true
        } catch {
            print("\(error)")
            
            return false
            // logg error to firebase
        }
    }
    
    func addOrUpdate<T: Object>(objects: [T]) -> Bool {
        do {
            let realm = try provider.realm()
            
            try realm.write {
                if !(T.primaryKey()?.isEmpty ?? false) {
                    realm.add(objects, update: .all)
                } else {
                    realm.add(objects, update: .error)
                }
            }
            return true
        } catch {
            print("\(error)")
            // logg error to firebase
            
            return false
        }
    }
    
    func get<T: Object>(id: Any) -> T? {
        do {
            let realm = try Realm(configuration: self.provider.configuration)
            // if used on background threads with no runLoop, as per official docs
            // https://realm.io/docs/swift/latest/#threading manual refresh is needed
            realm.refresh()
            return realm.object(ofType: T.self, forPrimaryKey: id)
        } catch {
            print("\(error)")
            // logg error to firebase
            return nil
        }
    }
    
    /**
        Delivers events only on queue with an active runLoop (main by default). If called on
        thread with no active runLoop exception is thrown; (Realm notifications cannot be received since there is no active runLoop)
    */
    func getAll<T: Object>() -> Results<T>? {
        do {
            let realm = try provider.realm()
            return realm.objects(T.self)
        } catch {
            print("\(error)")
            // logg error to firebase
            return nil
        }
    }
    
    func delete<T: Object>(object: T) -> Bool {
        do {
            let realm = try Realm(configuration: self.provider.configuration)
            try realm.write {
                realm.delete(object)
            }
            return true
        } catch {
            print("\(error)")
            // logg error to firebase
            return false
        }
    }
    
    func delete<T: Object>(id: Any, of type: T.Type) -> Bool {
        guard let object: T = get(id: id) else { return true }
        return delete(object: object)
    }
    
    func delete<T: Object>(objects: [T]) -> Bool {
        do {
            let realm = try Realm(configuration: self.provider.configuration)
            try realm.write {
                realm.delete(objects)
            }
            return true
        } catch {
            print("\(error)")
            // logg error to firebase
            
            return false
        }
    }
    
    func deleteAll<T: Object>(of type: T.Type) -> Bool {
        do {
            let realm = try Realm(configuration: self.provider.configuration)
            let objects = realm.objects(type)
            
            try realm.write {
                let objectsToDelete = Array(objects)
                realm.delete(objectsToDelete)
            }
            
            return true
        } catch {
            print("\(error)")
            // logg error to firebase
            
            return false
        }
    }
}

