//
//  RecipeRepositories.swift
//  FoodRecipes
//
//  Created by Diana Szalai on 21.06.2023.
//

import Foundation
import Combine
import RealmSwift

class RecipeRepository {
    
    private let chatGPTAPI: ChatGPTAPI
    private let realmProvider: RealmProviderType
    private let bkgScheduler: DispatchQueue
    
    init(chatGPTAPI: ChatGPTAPI,
         realmProvider: RealmProviderType,
         bkgScheduler: DispatchQueue = DispatchQueue(label: "UsersRepository-queue",
                                                     qos: .default,
                                                     attributes: .concurrent)) {
        self.chatGPTAPI = chatGPTAPI
        self.realmProvider = realmProvider
        self.bkgScheduler = bkgScheduler
    }
    
    func getRecipe(text: String,
                   recipeName: String,
                   servings: String,
                   cookTime: String,
                   recipeType: String) async -> RecipeObject? {
        do {
            let recipeResponse = try await chatGPTAPI.sendMessage(text)
            
            let recipeObj = mapToDomainModel(response: recipeResponse,
                                             recipeName: recipeName,
                                             servings: servings,
                                             cookTime: cookTime,
                                             recipeType: recipeType)
            
            let saveSuccess = save(recipe: recipeObj)
            
            if saveSuccess {
                let recipeObjCopy = RecipeObject(responseText: recipeObj.responseText,
                                                 recipeName: recipeObj.recipeName,
                                                 servings: recipeObj.servings,
                                                 cookTime: recipeObj.cookTime,
                                                 recipeType: recipeObj.recipeType,
                                                 createdAt: recipeObj.createdAt)
                return recipeObjCopy
            } else {
                return nil
            }
            
        } catch {
            print("Invalid recipe search")
            return nil
        }
    }
    
    func getRecipes() -> [RecipeObject] {
        let threadCache = DataCache(provider: realmProvider)
        
        let sortingProperties = [SortDescriptor(keyPath: "\(RecipeObject.Property.createdAt)", ascending: false)]
        
        let existingRecipes: Results<RecipeObject>? = threadCache.getAll()?.sorted(by: sortingProperties)
        
        if let existingRecipes {
            return Array(existingRecipes)
        } else {
            return []
        }
    }
    
    private func mapToDomainModel(response: String,
                                  recipeName: String,
                                  servings: String,
                                  cookTime: String,
                                  recipeType: String) -> RecipeObject {
        
        return RecipeObject(responseText: response,
                            recipeName: recipeName,
                            servings: servings,
                            cookTime: cookTime,
                            recipeType: recipeType,
                            createdAt: Date())
    }
        
    func save(recipe: RecipeObject) -> Bool {
        let threadCache = DataCache(provider: realmProvider)
        let saveResult = threadCache.addOrUpdate(newObject: recipe)
        print("recipe save successful: \(saveResult)")
        return saveResult
    }
    
    func getResponseFromCache() -> [RecipeObject] {
        let threadCache = DataCache(provider: realmProvider)
        let existingData: Results<RecipeObject>? = threadCache.getAll()
        
        if let existingData, !existingData.isInvalidated {
            return Array(existingData)
        } else {
            return []
        }
    }
}
