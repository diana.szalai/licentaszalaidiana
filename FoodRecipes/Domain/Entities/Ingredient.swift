//
//  Ingredient.swift
//  ListUsers
//
//  Created by Diana Szalai on 17.06.2023.
//

import Foundation

public struct Ingredient: Hashable, Codable, Identifiable {
    public var id = UUID()
    
    var quantity: String
    var name: String
    var emoji: String
}
