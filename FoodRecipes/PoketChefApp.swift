//
//  ListUsersApp.swift
//  ListUsers
//
//  Created by Diana Szalai on 27.05.2023.
//

import SwiftUI

@main
struct ListUsersApp: App {
    
    // MARK: Stored Properties
    
    @StateObject var coordinator = HomeCoordinator()
    @State private var showOnboarding = true
    // MARK: Scenes

    var body: some Scene {
        WindowGroup {
//            if showOnboarding {
//                AppOnboardingView(showOnboarding: $showOnboarding)
//            } else {
                HomeCoordinatorView(coordinator: coordinator)
//            }
        }
    }
}
