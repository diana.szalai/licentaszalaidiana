//
//  HomeCoordinatorView.swift
//  ListUsers
//
//  Created by Diana Szalai on 30.05.2023.
//

import SwiftUI

struct UsersAssistantCoordinatorView: View {
    
    // MARK: Stored Properties
    
    /// Controls the navigation within the `List` flow.
    @ObservedObject var usersAssistantCoordinator: UsersAssistantCoordinator
    
    // MARK: Views
    
    var body: some View {
        NavigationStack(path: $usersAssistantCoordinator.navigationPath) {
            if let viewModel = usersAssistantCoordinator.usersAssistantScreenViewModel {
                UsersAssistantScreen(viewModel: viewModel)
            }
        }
    }
}

