//
//  NetworkingUnitTest.swift
//  ListUsers
//
//  Created by Diana Szalai on 30.05.2023.
//

import Foundation
import XCTest

class URLProtocolStub: URLProtocol {
    // dictionary maps URLs to test data
    static var testURLs = [URL?: Data]()

    // handle all types of request
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    override func startLoading() {
        if let url = request.url {
            //use test data for that URL
            if let data = URLProtocolStub.testURLs[url] {
                self.client?.urlProtocol(self, didLoad: data)
            }
        }
        
        
        self.client?.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() { }
}
