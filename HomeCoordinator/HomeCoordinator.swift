//
//  HomeCoordinator.swift
//  ListUsers
//
//  Created by Diana Szalai on 28.05.2023.
//

import Foundation
import SwiftUI

enum HomeTab: Hashable {
    case chat
    case form
    case mealForm
    case search
}

class HomeCoordinator: ObservableObject {
    
    // MARK: Stored Properties
    @Published var tab = HomeTab.form
    @Published var navigationPath: NavigationPath = .init()
    
    // MARK: Child Coordinators
    private(set) var usersAssistantCoordinator: UsersAssistantCoordinator
    private(set) var formCoordinator: FormCoordinator
    private(set) var mealFormCoordinator: MealFormCoordinator
    private(set) var searchCoordinator: SearchCoordinator
    
    // MARK: Initialization
    init() {
        let realmUsersConfig = RealmProvider.usersConfig
        let realmProvider = RealmProvider(configuration: realmUsersConfig)
        
        let chatGPTAPI = ChatGPTAPI(realmProvider: realmProvider)
        
        let recipeRepository = RecipeRepository(chatGPTAPI: chatGPTAPI,
                                                realmProvider: realmProvider)
        
        self.usersAssistantCoordinator = UsersAssistantCoordinator(
            chatGPTAPI: chatGPTAPI
        )
        self.formCoordinator = FormCoordinator(
            recipeRepository: recipeRepository
        )
        
        self.mealFormCoordinator = MealFormCoordinator(
            chatGPTAPI: chatGPTAPI
        )
        self.searchCoordinator = SearchCoordinator(
            recipeRepository: recipeRepository,
            formCoordinator: formCoordinator
        )
    }
    
    func switchToTabHome() {
        self.tab = .chat
    }
}
