//
//  HomeCoordinatorView.swift
//  ListUsers
//
//  Created by Diana Szalai on 29.05.2023.
//

import SwiftUI

struct HomeCoordinatorView: View {
    
    // MARK: Stored Properties
    
    @ObservedObject var coordinator: HomeCoordinator
    @Environment(\.horizontalSizeClass) var sizeClass
    
    // MARK: Views
    
    var body: some View {
        switch sizeClass  {
        case .compact:
            TabView(selection: $coordinator.tab) {
                UsersAssistantCoordinatorView(usersAssistantCoordinator: coordinator.usersAssistantCoordinator)
                    .tabItem {
                        VStack {
                            Text("Chat")
                            Image.characterImage
                                .renderingMode(.template)
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(.green)
                        }
                    }
                    .tag(HomeTab.chat)
                FormCoordinatorView(formCoordinator: coordinator.formCoordinator)
                    .tabItem {
                        VStack {
                            Text("Form")
                            Image.formImage
                                .renderingMode(.template)
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(.green)
                        }
                    }
                    .tag(HomeTab.form)
                MealFormCoordinatorView(mealFormCoordinator: coordinator.mealFormCoordinator)
                    .tabItem {
                        VStack {
                            Text("Plan")
                            Image.calendarImage
                                .renderingMode(.template)
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(.green)
                        }
                    }
                    .tag(HomeTab.mealForm)
                SearchCoordinatorView(searchCoordinator: coordinator.searchCoordinator)
                    .tabItem {
                        VStack {
                            Text("Search")
                            Image.searchImage
                                .renderingMode(.template)
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(.green)
                        }
                    }
                    .tag(HomeTab.search)
            }
        default:
            EmptyView()
        }
    }
}

public extension Image {
    
    // MARK: - TabBar
    static let characterImage = Image(systemName: "text.bubble")
    static let calendarImage = Image(systemName: "calendar")
    static let formImage = Image(systemName: "doc.text")
    static let searchImage = Image(systemName: "magnifyingglass")
    static let formImageMultiColor = Image(systemName: "fork.knife.circle.fill")
}
