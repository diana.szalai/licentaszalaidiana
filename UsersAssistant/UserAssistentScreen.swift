//
//  UserAssistentScreen.swift
//  ListUsers
//
//  Created by Diana Szalai on 19.06.2023.
//

import SwiftUI

struct UsersAssistantScreen: View {
    @ObservedObject var viewModel: UsersAssistantScreenViewModel
    
    var body: some View {
        if let vm = viewModel.chatGPTViewModel {
            ChatGPTView(vm: vm)
        }
    }
}

