//
//  UserAssistentScreenViewModel.swift
//  ListUsers
//
//  Created by Diana Szalai on 19.06.2023.
//

import SwiftUI

class UsersAssistantScreenViewModel: ObservableObject {
    
    @Published var chatGPTViewModel: ChatGPTViewModel?
    
    private weak var usersAssistantCoordinator: UsersAssistantCoordinator?
    private let api: ChatGPTAPI
    
    init(api: ChatGPTAPI,
         usersAssistantCoordinator: UsersAssistantCoordinator,
         chatGPTViewModel: ChatGPTViewModel) {
             
        self.api = api
        self.usersAssistantCoordinator = usersAssistantCoordinator
        self.chatGPTViewModel = ChatGPTViewModel(api: api)
    }

}

