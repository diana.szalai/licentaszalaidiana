//
//  RecipeDetailScreenViewModel.swift
//  ListUsers
//
//  Created by Diana Szalai on 16.06.2023.
//

import Combine
import Foundation

class RecipeDetailScreenViewModel: ObservableObject {
    
    @Published var recipeResponse: [MessageRow] = []
    @Published var viewState: ViewState = .default
    @Published var finalMessage: [String] = []
    @Published var preparationSteps: [String] = []
    
    @Published var recipeInfo: RecipeInfo?
    @Published var recipeName: String
    @Published var ingredients: [Ingredient]?
    @Published var cookTime: String
    @Published var servings: String
    
    weak var formCoordinator: FormCoordinator?
    private var task: Task<Void, Never>?
    
    init(recipeResponse: [MessageRow],
         recipeName: String,
         cookTime: String,
         servings: String,
         formCoordinator: FormCoordinator?) {
        
        self.recipeResponse = recipeResponse
        self.recipeName = recipeName
        self.cookTime = cookTime
        self.servings = servings
        self.formCoordinator = formCoordinator
                
        if recipeResponse.isEmpty {
            viewState = .redactAndShimmer
        } else {
            viewState = .default
        }
        
        editResponse()
    }
    
    func editResponse() {
        recipeResponse.forEach{
            self.finalMessage.append( $0.response?.text ?? "")
        }
        
        guard let ingredientsText = finalMessage.first else { return }
        let result = ingredientsText
            .components(separatedBy: "\n- ")
            .filter({ !$0.contains("Ingredients:")})
            .prefix { $0.contains("Preparation:") ? !$0.contains("Preparation:") : !$0.contains("Preparation Steps:") }
    
        guard let stepsText = finalMessage.first else { return }
        
        let preparationStepsIndex = stepsText
            .components(separatedBy: "\n")
            .firstIndex(of: "Preparation Steps:")
        
        let preparationIndex = stepsText
            .components(separatedBy: "\n")
            .firstIndex(of: "Preparation:")
 
        let array = stepsText
            .components(separatedBy: "\n")
        
        let removeThis = preparationStepsIndex ?? preparationIndex ?? 1
        let totalArrayItems = array.count - removeThis
        self.preparationSteps = array.suffix(totalArrayItems)
        
        self.ingredients = result.map{ Ingredient(quantity: "", name: $0, emoji: "")}
                
        self.recipeInfo = RecipeInfo(name: recipeName,
                                     ingredients: ingredients ?? [],
                                     cookTime: cookTime,
                                     servings: servings,
                                     steps: preparationSteps,
                                     imageName: "")
        
        print(">>>>>>>ingredients, \(String(describing: ingredients))")
        print(">>>>>>>recipies, \(String(describing: recipeInfo))")
    }
}
