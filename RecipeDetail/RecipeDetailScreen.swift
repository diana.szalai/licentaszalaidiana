//
//  RecipeDetailScreen.swift
//  ListUsers
//
//  Created by Diana Szalai on 16.06.2023.
//


import SwiftUI

struct RecipeDetailScreen: View {
    @ObservedObject var viewModel: RecipeDetailScreenViewModel
    @State private var stepsShowing = false
    @Environment(\.colorScheme) var colorScheme
    
    init(viewModel: RecipeDetailScreenViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ScrollView {
            if let recipe = viewModel.recipeInfo {
                VStack{
                    DescHeaderView(stepsShowing: $stepsShowing, recipe: recipe)
                    if stepsShowing {
                        VStack(spacing: 8) {
                            HStack(alignment: .lastTextBaseline) {
                                Text("Preparation Steps:")
                                    .font(.title2)
                                    .padding([.top, .bottom], 30)
                                Spacer()
                                Text("\(viewModel.preparationSteps.count) steps")
                                    .font(.callout)
                                    .foregroundColor(.secondary)
                            }
                            ForEach(recipe.steps, id: \.self) { step in
                                HStack(alignment: .center) {
                                    Text(step)
                                    Spacer()
                                }
                            }
                        }
                    } else {
                        VStack(spacing: 8) {
                            HStack(alignment: .lastTextBaseline) {
                                Text("Ingredients")
                                    .font(.title2)
                                    .padding([.top, .bottom], 30)
                                Spacer()
                                Text("\(recipe.ingredients.count) item")
                                    .font(.callout)
                                    .foregroundColor(.secondary)
                            }
                            ForEach(recipe.ingredients, id: \.id) { ingredient in
                                HStack(alignment: .center) {
                                    Text(ingredient.name)
                                    Spacer()
                                }
                            }
                        }
                    }
                }.padding([.all], 10)
            }
        }.background(colorScheme == .light ? .white : Color(red: 52/255, green: 53/255, blue: 65/255, opacity: 0.5))
    }
}


func messageResponseRow(rowType: MessageRowType,
                        bgColor: Color) -> some View {
    HStack(alignment: .top, spacing: 8) {
        VStack(alignment: .leading) {
            if !rowType.text.isEmpty {
                Text(rowType.text)
                    .multilineTextAlignment(.leading)
                    .textSelection(.enabled)
            }
        }
    }
    .padding(12)
    .frame(maxWidth: .infinity, alignment: .leading)
    .background(bgColor)
}
