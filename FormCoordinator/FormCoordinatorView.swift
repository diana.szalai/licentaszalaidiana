//
//  FormCoordinator.swift
//  ListUsers
//
//  Created by Diana Szalai on 15.06.2023.
//

import SwiftUI

struct FormCoordinatorView: View {
    
    @ObservedObject var formCoordinator: FormCoordinator
    
    // MARK: Views
    var body: some View {
        NavigationStack(path: $formCoordinator.navigationPath) {
            if let viewModel = formCoordinator.formRecipesViewModel {
                FormRecipesScreen(viewModel: viewModel)
                    .handleFormNavigation(coordinator: formCoordinator)
            }
        }
    }
}

extension View {
    func handleFormNavigation(coordinator: FormCoordinator) -> some View {
        navigationDestination(for: FormRoute.self) { route in
            switch route {
            case .recipeDetails(let recipe, let recipeName, let cookTime, let servings):
                let viewModel = RecipeDetailScreenViewModel(recipeResponse: recipe,
                                                            recipeName: recipeName,
                                                            cookTime: cookTime,
                                                            servings: servings,
                                                            formCoordinator: coordinator)
                RecipeDetailScreen(viewModel: viewModel)
            }
        }
    }
}
