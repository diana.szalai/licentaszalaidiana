//
//  FormCoordinator.swift
//  ListUsers
//
//  Created by Diana Szalai on 15.06.2023.
//

import Foundation
import SwiftUI

enum FormRoute: Hashable {
    /// Details page of an item in which more details are visible and rating page.
    case recipeDetails(recipeResponse: [MessageRow], recipeName: String, cookTime: String, servings: String)
}

class FormCoordinator: ObservableObject {
    
    // MARK: - Properties
    private(set) var formRecipesViewModel: FormRecipesScreenViewModel?
    private(set) var recipeRepository: RecipeRepository

    /// Navigation
    @Published var navigationPath: NavigationPath = .init()
    
    // MARK: - Init
    
    init(recipeRepository: RecipeRepository) {
        self.recipeRepository = recipeRepository
        self.formRecipesViewModel = FormRecipesScreenViewModel(
            recipeRepository: recipeRepository,
            formCoordinator: self)
    }

    // MARK: Methods to be trigger when a certain event occurs
    
    func showRecipeDetails(recipeResponse: [MessageRow],
                           recipeName: String,
                           cookTime: String,
                           servings: String) {
        navigationPath.append(FormRoute.recipeDetails(
            recipeResponse: recipeResponse,recipeName: recipeName, cookTime: cookTime, servings: servings)
        )
    }
}
