//
//  RecipeListView.swift
//  FoodRecipes
//
//  Created by Diana Szalai on 21.06.2023.
//

import SwiftUI

struct RecipeListView: View {
    
    @ObservedObject var viewModel: RecipeListScreenViewModel
    @State var isShowing = false
    var recipes: [RecipeInfo] {
        if viewModel.searchText.isEmpty {
            return viewModel.allRecipes
        }
        
        return viewModel.allRecipes.filter({ $0.name.lowercased().contains(viewModel.searchText.lowercased())})
    }
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                SearchView(searchText: $viewModel.searchText)
                ScrollView(showsIndicators: false) {
                    LazyVStack(alignment: .leading, spacing:  10) {
                        BannerView()
                        ForEach(recipes, id: \.self) { recipe in
                            NavigationLink(destination: RecipeDetailView(recipe: recipe,
                                                                         isShowing: $isShowing)) {
                                ZStack {
                                    Image(recipe.imageName)
                                        .resizable()
                                        .scaledToFill()
                                        .overlay(LinearGradient(colors: [.black.opacity(0.5), .clear, .black.opacity(0.5)], startPoint: .topLeading, endPoint: .bottomTrailing))
                                        .frame(width: UIScreen.main.bounds.width * 0.9, height: 300)
                                        .clipped()
                                    VStack(alignment: .leading) {
                                        Text(recipe.recipeType.capitalized)
                                            .padding(.horizontal)
                                            .padding(.vertical, 10)
                                            .background(.thinMaterial)
                                            .clipShape(Capsule())
                                        Spacer()
                                        HStack(alignment: .top) {
                                            VStack(alignment: .leading, spacing: 20) {
                                                Text(recipe.name)
                                                    .font(.title)
                                                Text("\(recipe.cookTime) | \(recipe.servings)")
                                                    .foregroundColor(.secondary)
                                            }
                                            .frame(maxWidth: UIScreen.main.bounds.width * 0.8, alignment: .leading)
                                        }
                                        .padding()
                                        .background(.thinMaterial, in: RoundedRectangle(cornerRadius: 20))
                                    }
                                    .padding()
                                }
                                .clipShape(RoundedRectangle(cornerRadius: 20))
                            }
                                                                         .buttonStyle(.plain)
                        }
                    }
                }
            }
            .animation(.easeInOut, value: viewModel.searchText)
            .padding(.horizontal)
            .navigationTitle("History Recipes")
        }
    }
}
