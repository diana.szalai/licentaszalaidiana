//
//  RecipeListScreenViewModel.swift
//  FoodRecipes
//
//  Created by Diana Szalai on 21.06.2023.
//
import Combine
import Foundation

class RecipeListScreenViewModel: ObservableObject {
    
    @Published var searchText = ""
    @Published var allRecipes: [RecipeInfo] = []
    @Published var preparationSteps: [String] = []
    @Published var ingredients: [Ingredient]?
    
    @Published var viewState: ViewState = .default
    @Published var recipeInfo: RecipeInfo?
    @Published var recipeName: String = ""
    @Published var cookTime: String = ""
    @Published var servings: String = ""
    @Published var recipeResponse: [MessageRow] = []
    @Published var recipeDetailScreenViewModel: RecipeDetailScreenViewModel?
    
    private var task: Task<Void, Never>?
    private weak var searchCoordinator: SearchCoordinator?
    private weak var formCoordinator: FormCoordinator?
    private let recipeRepository: RecipeRepository
    
    init(recipeRepository: RecipeRepository,
         searchCoordinator: SearchCoordinator?,
         formCoordinator: FormCoordinator?) {
        
        self.recipeRepository = recipeRepository
        self.searchCoordinator = searchCoordinator
        
        self.task = Task {
            await getAllRecipes()
        }
    }
    
    func getAllRecipes() async {
        var messageRow = MessageRow(
            isInteracting: true,
            sendImage: "profile",
            send: .rawText("text"),
            responseImage: "openai",
            response: .rawText(""),
            responseError: nil)
        
        do {
            let recipes = recipeRepository.getResponseFromCache()
            
            self.allRecipes = recipes.compactMap{
                messageRow.response = .rawText($0.responseText)
                self.recipeResponse.append(messageRow)
                
                let ingredientsText = $0.responseText
                let result = ingredientsText
                    .components(separatedBy: "\n- ")
                    .filter({ !$0.contains("Ingredients:")})
                    .prefix { $0.contains("Preparation:") ? !$0.contains("Preparation:") : !$0.contains("Preparation Steps:") }
                self.ingredients = result.map{ Ingredient(quantity: "", name: $0, emoji: "")}
                let stepsText = $0.responseText
                
                let preparationStepsIndex = stepsText
                    .components(separatedBy: "\n")
                    .firstIndex(of: "Preparation Steps:")
                
                let preparationIndex = stepsText
                    .components(separatedBy: "\n")
                    .filter{ !$0.isEmpty }
                    .firstIndex(of: "Preparation:")
                
                let array = stepsText
                    .components(separatedBy: "\n")
                    .filter{ !$0.isEmpty }
                
                let removeThis = preparationStepsIndex ?? preparationIndex ?? 1
                let totalArrayItems = array.count - removeThis
                self.preparationSteps = array.suffix(totalArrayItems)
                
                return RecipeInfo(
                    name: $0.recipeName,
                    recipeType: $0.recipeType,
                    ingredients: ingredients ?? [],
                    cookTime: $0.cookTime,
                    servings: $0.servings,
                    steps: preparationSteps)
            }
        }
    }
}

